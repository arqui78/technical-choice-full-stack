FROM node:14
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PORT=8000
ENV API_BASE_URL="http://localhost:8000"
COPY package*.json ./
COPY yarn.lock ./
COPY ./packages/server/package.json packages/server/
COPY ./packages/web/package.json packages/web/
RUN yarn install
COPY . .
EXPOSE 8000
CMD ["yarn", "start"]
