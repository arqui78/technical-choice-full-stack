/**
 * Dependencies
 * */
import express from 'express'
import path from 'path'
import { errorHandler, errorNotFound } from './utils'
import routes from './routers'
import * as controllers from './controllers'
import * as middlewares from './middlewares'

const app = express()
const __dirname = path.resolve()
const optsCache = {
  etag: true, // Just being explicit about the default.
  lastModified: true, // Just being explicit about the default.
  setHeaders: (res, path) => {
    const hashRegExp = new RegExp('\\.[0-9a-f]{8}\\.')

    if (path.endsWith('.html')) {
      // All of the project's HTML files end in .html
      res.set('Cache-Control', 'no-cache')
    } else if (hashRegExp.test(path)) {
      // If the RegExp matched, then we have a versioned URL.
      res.set('Cache-Control', 'max-age=31536000')
    }
  }
}

app.enable('trust proxy') // only if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
app.set('trust proxy', 1) // trust first proxy

//disables powered by express
app.disable('x-powered-by')
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use(function (req, res, next) {
  res.header('access-control-allow-origin', '*')
  res.header(
    'access-control-allow-headers',
    'Origin, X-Requested-With, Content-Type, Range, Content-Range, Accept, Authorization, Accept-Language, cache-control'
  )
  res.header(
    'access-control-allow-methods',
    'POST, GET, PUT, PATCH, DELETE, OPTIONS'
  )
  res.header('access-control-expose-headers', 'Content-Type, Range, Method')
  next()
})
// Render View
app.use(express.static(path.join(__dirname, '..', 'web', 'dist'), optsCache))

// API
app.use(routes(express.Router(), controllers, middlewares))

// catch 404 and forward to error handler
app.use(errorNotFound)
// error handler
app.use(errorHandler)

export default app
