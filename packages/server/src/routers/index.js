import iEchoRouters from './iEchoRouters'

const routers = (router, controllers, middlewares) => {
  const { iEchoController } = controllers
  const { iEchoMiddleware } = middlewares
  router.use('/iecho', iEchoRouters(router, iEchoController, iEchoMiddleware))
  return router
}

export default routers
