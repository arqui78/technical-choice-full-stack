const iEchoRouters = (router, iEchoController, iEchoMiddleware) => {
  const { runReverseText } = iEchoController
  const { iEchoValidateQuery } = iEchoMiddleware

  router.get('/', [iEchoValidateQuery], runReverseText)

  return router
}

export default iEchoRouters
