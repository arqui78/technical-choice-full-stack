import debug from 'debug'
debug('api:*')
/**
 * Normalize a port into a number, string, or false.
 */
const normalizePort = (val) => {
  let port = parseInt(val, 10)

  if (isNaN(port)) {
    // named pipe
    return val
  }

  if (port >= 0) {
    // port number
    return port
  }

  return false
}

/**
 * Event listener for HTTP server "error" event.
 */

const onError = (error, port) => {
  if (error.syscall !== 'listen') {
    throw error
  }

  let bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`)
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`)
      process.exit(1)
      break
    default:
      throw error
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

const onListening = (server) => {
  let addr = server.address()
  let bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`
  debug(`Listening on ${bind}`)
}
const errorHandler = (err, req, res) => {
  res.status(err.status || 500)
  res.json({
    code: 500,
    message: err.message || 'Internal Server',
    description: err.toString()
  })
}

const errorNotFound = (req, res) => {
  const err = new Error('Not Found')
  err.status = 404
  res.status(err.status)
  res.json({
    code: err.status,
    message: err.message
  })
}

/**
 * @function reverseTextUtil
 * @description Returns the text received as a reversed argument
 * @param text
 * @return reversedText
 */
const reverseTextUtil = (text) => text.split('').reverse().join('')

function connectServer(app, port) {
  return app.listen(port)
}

export {
  normalizePort,
  onError,
  onListening,
  errorHandler,
  errorNotFound,
  reverseTextUtil,
  connectServer
}
