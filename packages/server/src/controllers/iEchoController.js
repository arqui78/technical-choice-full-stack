import { reverseTextUtil } from '../utils'

const runReverseText = (req, res) => {
  const { text } = req.query
  return res.json({
    _id: Date.now(),
    reversedText: reverseTextUtil(text),
    createdAt: new Date(),
    text
  })
}

export { runReverseText }
