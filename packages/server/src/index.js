import dotenv from 'dotenv'
import app from './app'
import { normalizePort } from './utils'

dotenv.config()
const PORT = normalizePort(process.env.PORT || 8000)
app.listen(PORT, function () {
  console.log(`[App]: Server listening on ${PORT}`)
})
