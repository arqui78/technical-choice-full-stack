import Joi from 'joi'
const Schema = Joi.object({
  text: Joi.string().required()
})

const iEchoValidateQuery = (req, res, next) => {
  const { error } = Schema.validate(req.query)

  if (error) {
    return res.status(400).json({
      error: 'Text invalid',
      description: error
    })
  }

  next()
}

export { iEchoValidateQuery }
