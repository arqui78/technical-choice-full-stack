import 'regenerator-runtime/runtime'
import request from 'supertest'
import app from '../app'
import { reverseTextUtil, connectServer } from '../utils'

let server
describe('GET /iecho', () => {
  beforeAll(async () => {
    try {
      server = connectServer(app, 8000)
    } catch (error) {
      console.log(error)
    }
  })

  it('should return reversed text', (done) => {
    const text = 'este texto se debe invertir'
    const expected = reverseTextUtil(text)

    request(server)
      .get(`/iecho?text=${text}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          console.error(err)
          return done(err)
        }

        expect(res.body).toHaveProperty('reversedText') // inject by jest

        const { reversedText = '' } = res.body
        expect(reversedText).toEqual(expected)
        done()
      })
  })

  afterAll((done) => {
    server.close()
    done()
  })
})
