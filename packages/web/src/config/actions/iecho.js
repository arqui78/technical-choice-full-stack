import { iEchoApi } from '../api'

export function get(params) {
  return {
    type: iEchoApi.get.type,
    payload: {
      request: {
        ...iEchoApi.get.payload.request,
        params: params || {}
      }
    }
  }
}

export function remove(_id) {
  return {
    type: iEchoApi.remove.type,
    _id
  }
}
