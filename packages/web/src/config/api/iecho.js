const iEcho = {
  get: {
    type: 'GET_IECHO',
    start: 'GET_IECHO_START',
    success: 'GET_IECHO_SUCCESS',
    error: 'GET_IECHO_FAIL',
    payload: {
      request: {
        method: 'GET',
        url: `${process.env.API_BASE_URL}/iecho`
      }
    }
  },
  remove: {
    type: 'DELETE_IECHO'
  }
}
export default iEcho
