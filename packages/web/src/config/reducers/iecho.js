import { iEchoApi } from '../api'

const initialState = {
  isSending: false,
  results: []
}

export const iEchoReducer = (state = initialState, action) => {
  switch (action.type) {
    case iEchoApi.get.start:
      return {
        ...state,
        isSending: true
      }
    case iEchoApi.get.success:
      return {
        ...state,
        isSending: false,
        results: [action.payload.data, ...state.results]
      }

    case iEchoApi.get.error:
      return {
        ...state,
        isSending: false
      }
    case iEchoApi.remove.type:
      return {
        ...state,
        results: state.results.filter((item) => item._id !== action._id)
      }
    default:
      return state
  }
}

export default iEchoReducer
