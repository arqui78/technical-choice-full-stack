import { combineReducers } from 'redux'

import iEcho from './iecho'

const reducers = combineReducers({
  iEcho
})

export default reducers
