import thunk from 'redux-thunk'
import axiosMiddleware from 'redux-axios-middleware'
import promiseMiddleware from 'redux-promise-middleware'
import { createStore, applyMiddleware } from 'redux'
import { create } from 'axios'

const api = create()

export default function configureStore(reducers, options) {
  const { initialState = {} } = options

  const middleware = [
    thunk,
    axiosMiddleware(api),
    promiseMiddleware({
      promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR']
    })
  ]

  return createStore(reducers, initialState, applyMiddleware(...middleware))
}
