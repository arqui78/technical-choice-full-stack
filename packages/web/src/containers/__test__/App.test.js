import React from 'react'
import { render } from '@testing-library/react'
import App from '../App'
import { Provider } from 'react-redux'
import configStore from '../../config/store'
import reducers from '../../config/reducers'

test('App', () => {
  const store = configStore(reducers, {})
  render(
    <Provider store={store}>
      <App />
    </Provider>
  )
})
