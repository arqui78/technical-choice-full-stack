import './App.css'
import Header from '../components/Header'
import ResultsList from '../components/ResultsList'
import { connect } from 'react-redux'
import * as actions from './actions'

function App({ results, getIEcho, removeIEcho }) {
  return (
    <div className="App">
      <Header getIEcho={getIEcho} />
      <ResultsList results={results} removeIEcho={removeIEcho} />
    </div>
  )
}
const mapStateToProps = (state) => {
  const { iEcho } = state
  return {
    results: iEcho.results
  }
}
export default connect(mapStateToProps, actions)(App)
