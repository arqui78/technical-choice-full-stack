import { iEchoAction } from '../config/actions'

export function getIEcho(params) {
  return iEchoAction.get(params)
}

export function removeIEcho(_id) {
  return iEchoAction.remove(_id)
}
