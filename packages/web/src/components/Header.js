import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

const Header = ({ getIEcho }) => {
  const [disabled, setDisabled] = useState(true)
  const [value, setValue] = useState('')
  const onSubmit = async (event) => {
    event.preventDefault()
    await getIEcho({ text: value })
    setValue('')
  }
  useEffect(() => {
    setDisabled(value ? false : true)
  }, [value])
  return (
    <nav className="navbar navbar-light">
      <form className="d-flex" autoComplete="off" onSubmit={onSubmit}>
        <input
          className="form-control me-3 text-field"
          type="text"
          placeholder="Insert Text"
          aria-label="insert-text"
          autoFocus
          value={value}
          onChange={(event) => setValue(event.target.value)}
        />
        <button className="btn btn-send" type="submit" disabled={disabled}>
          Send
        </button>
      </form>
    </nav>
  )
}

Header.propTypes = {
  getIEcho: PropTypes.func.isRequired
}
export default Header
