import PropTypes from 'prop-types'

const ResultsList = ({ results, removeIEcho }) => {
  return (
    <div className="container">
      <div className="d-flex justify-content-start py-4 text-results">
        Results:
      </div>
      <div className="content-list">
        {results.length > 0 ? (
          results.map((item, key) => (
            <div className="item-list" key={key}>
              <div
                className="float-end px-2 item-deleted"
                onClick={() => removeIEcho(item._id)}
              >
                <div className="item-deleted-pointer">X</div>
              </div>
              {item.reversedText || ''}
            </div>
          ))
        ) : (
          <div className="text-center">No hay resultados que mostrar</div>
        )}
      </div>
    </div>
  )
}

ResultsList.propTypes = {
  results: PropTypes.array.isRequired,
  removeIEcho: PropTypes.func.isRequired
}

export default ResultsList
