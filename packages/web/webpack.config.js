/**
 * Dependencies
 */
const path = require("path");
const webpack = require("webpack");
const merge = require("webpack-merge");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const Dotenv = require("dotenv-webpack");

require("dotenv").config();

const paths = {
	src: path.resolve(__dirname, "./src"),
	public: path.resolve(__dirname, "./public"),
	build: path.resolve(__dirname, "./dist")
};

let htmlWebPackPluginOpts = {
	title: "React App",
	favicon: `${paths.public}/favicon.ico`,
	template: `${paths.public}/index.html`,
	filename: "index.html",
}

const globals = {
	"process.env": {
		API_BASE_URL: JSON.stringify(process.env.API_BASE_URL)
	}
};

let commons = {
	plugins: [
		new Dotenv({systemvars: true}),
		new HtmlWebPackPlugin( htmlWebPackPluginOpts),
		new webpack.ProvidePlugin({
			React: "react"
		})
	],
	module: {
		rules: [
			{
				test: /\.js$|jsx/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$/,
				use: [
					"style-loader",
					"raw-loader",
					MiniCssExtractPlugin.loader,
					"css-loader"
				]
			},
			{
				test: /\.scss$/,
				use: [
					"style-loader",
					'css-loader',
					'sass-loader'
				]
			},
			{
				test: /\.(webp|jpg|jpeg|png|svg)$/,
				use: [
					{
						loader: "url-loader",
						options: {
							prefix: "image",
							limit: 5000,
							name: '[name].[ext]'
						}
					}
				]
			},
			{
				test: /\.(woff|woff2|eot|otf|ttf)$/,
				use: [
					{
						loader: "url-loader",
						options: {
							prefix: "font",
							limit: 5000,
							name: '[name].[ext]'
						}
					}
				]
			}
		]
	},
	output: {
		publicPath: "/"
	}
};

let development = {
	mode: "development",
	devtool: "cheap-module-eval-source-map",
	plugins: [
		new MiniCssExtractPlugin({
			filename: "[name].bundle.css"
		})
	],
	devServer: {
		historyApiFallback: true,
		contentBase: paths.public,
		hot: true,
		port: process.env.PORT
	},
	output: {
		filename: "[name].bundle.js"
	}
};

let production = {
	mode: "production",
	devtool: "source-map",
	plugins: [
		new CleanWebpackPlugin(),
		new webpack.DefinePlugin(globals),
		new MiniCssExtractPlugin({
			filename: "[name]-[hash].bundle.css"
		}),
		new CopyPlugin([
			{ from: `${paths.public}/robots.txt`, to: paths.build },
			{ from: `${paths.public}/logo192.png`, to: paths.build },
			{ from: `${paths.public}/logo512.png`, to: paths.build },
			{ from: `${paths.public}/manifest.json`, to: paths.build },
		]),
		new webpack.optimize.AggressiveMergingPlugin()
	],
	optimization: {
		minimizer: [
			new TerserPlugin({
				parallel: true,
				sourceMap: true,
				cache: true
			}),
			new OptimizeCSSAssetsPlugin({
				cssProcessorOptions: {
					map: {
						inline: false,
						annotation: true
					}
				}
			})
		],
		splitChunks: {
			chunks: "all"
		},
		runtimeChunk: {
			name: entrypoint => `runtimechunk~${entrypoint.name}`
		}
	},
	performance: {
		hints: false,
		maxEntrypointSize: 512000,
		maxAssetSize: 512000
	},
	output: {
		filename: "[name]-[hash].bundle.js"
	}
};

module.exports = (_env, argv) => {
	if (argv.mode === "production") return merge(commons, production);

	return merge(commons, development);
};
