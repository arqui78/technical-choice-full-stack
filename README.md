# Technical Choice Full Stack

---

### What do you need to execute this app on your computer?

-   [node.js] - evented I/O for the backend

```sh
curl -sL https://deb.nodesource.com/setup_14.x | apt-get install -y nodejs

apt-get install -y build-essential
```

### Installation

first clone the repository

```sh
$ git clone https://gitlab.com/arqui78/technical-choice-full-stack.git
```

enter to cloned folder

```sh
 cd technical-choice-full-stack
```

### How to run it

-   [npm] - First you need to install all required node modules, so execute this

```sh
$ yarn install
```

---

# Running test

```sh
$ yarn test
```

# Running the app

```sh
$ yarn start
```

## Production

### Compile for production:

I completed your files before uploading to production

```sh
$ yarn start
```
### Deploy with Docker:

Build images

```sh
$ docker build . -t tc-full-stack
```
Run Container
```sh
$ docker run -p 8000:8000 -d tc-full-stack
```
[node.js]: http://nodejs.org
[npm]: https://www.npmjs.com/
